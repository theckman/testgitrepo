// Package testgitrepo is used to test https://github.com/golang/dep
package testgitrepo

// Package is just a random string to have something to render in the GoDoc.
const Package = "https://bitbucket.org/theckman/testgitrepo"
